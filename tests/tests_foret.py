import unittest
from enum import Enum

from app import (
    countFireAround,
    newCaseState,
    getColor,

)

forestMap = []

class Type(Enum):
    EMPTY = 0
    TREE = 1
    FIRE = 2
    ASHES = 3

forestMap[2][1] = Type.FIRE
class Testapp(unittest.TestCase):
    def test_getColor_not_blue(self):
        self.assertNotEqual(getColor(Type.TREE), 'blue')
        self.assertNotEqual(getColor(Type.FIRE), 'blue')
        self.assertNotEqual(getColor(Type.EMPTY), 'blue')
        self.assertNotEqual(getColor(Type.ASHES), 'blue')

    def test_getColor_not_TREE(self):
        self.assertNotEqual(getColor(Type.TREE), "#FF2121")
        self.assertNotEqual(getColor(Type.TREE), "#65DE46")
        self.assertNotEqual(getColor(Type.TREE), "#7B7B7B")
    
    def test_getColor_not_FIRE(self):
        self.assertNotEqual(getColor(Type.FIRE), "#01C639")
        self.assertNotEqual(getColor(Type.FIRE), "#65DE46")
        self.assertNotEqual(getColor(Type.FIRE), "#7B7B7B")

    def test_getColor_not_EMPTY(self):
        self.assertNotEqual(getColor(Type.EMPTY), "#01C639")
        self.assertNotEqual(getColor(Type.EMPTY), "#FF2121")
        self.assertNotEqual(getColor(Type.EMPTY), "#7B7B7B")
    
    def test_getColor_not_ASHES(self):
        self.assertNotEqual(getColor(Type.ASHES), "#01C639")
        self.assertNotEqual(getColor(Type.ASHES), "#FF2121")
        self.assertNotEqual(getColor(Type.ASHES), "#65DE46")

    def test_countFireAround(self):
        self.assertEqual(countFireAround(2, 2), 1)