from enum import Enum
from tkinter import *
import random
import datetime
import math
import argparse

master = Tk(screenName="Simulation de feux de foret")
caseSize = 0
nbCol = 0
nbLine = 0
forestMap = []
window = None
mustQuit = False

class Type(Enum):
    EMPTY = 0
    TREE = 1
    FIRE = 2
    ASHES = 3

def createMap(afforestation):
    global forestMap
    for y in range(0, nbCol):
        forestMap.append([])
        for x in range(0, nbLine):
            if (random.random() < afforestation):
                forestMap[y].append(Type.TREE)
            else:
                forestMap[y].append(Type.EMPTY)

def countFireAround(x, y):
    count = 0
    if (x - 1 >= 0 and forestMap[y][x - 1] == Type.FIRE):
        count += 1
    if (x + 1 < nbLine and forestMap[y][x + 1] == Type.FIRE):
        count += 1
    if (y - 1 >= 0 and forestMap[y - 1][x] == Type.FIRE):
        count += 1
    if (y + 1 < nbCol and forestMap[y + 1][x] == Type.FIRE):
        count += 1
    return count

def newCaseState(x, y, rule):
    global forestMap
    state = forestMap[y][x]
    if (state == Type.ASHES):
        return(Type.EMPTY)
    elif (state == Type.FIRE):
        return(Type.ASHES)
    elif (state == Type.TREE):
        nbFireAround = countFireAround(x, y)
        if (rule == 0):
            if (nbFireAround > 0):
                return(Type.FIRE)
            else:
                return(Type.TREE)
        else:
            fireChance = 1 - (1 / (nbFireAround + 1))
            if (random.random() < fireChance):
                return(Type.FIRE)
            else:
                return(Type.TREE)

def checkMap(rule):
    global forestMap
    new_map = []
    for y in range(0, nbCol):
        new_map.append([])
        for x in range(0, nbLine):
            new_map[y].append(newCaseState(x, y, rule))
    forestMap = new_map

def getColor(state):
    if (state == Type.TREE):
        return "#01C639"
    elif (state == Type.FIRE):
        return "#FF2121"
    elif (state == Type.EMPTY):
        return "#65DE46"
    elif (state == Type.ASHES):
        return "#7B7B7B"

def drawMap():
    for y in range(0, nbCol):
        for x in range(0, nbLine):
            drawCase(x, y, getColor(forestMap[y][x]))

def drawCase(x, y, color):
    window.create_rectangle(x * caseSize, y * caseSize, (x + 1) * caseSize, (y + 1) * caseSize, fill=color)

nextUpdate = 0
def launchSim(rule, animTime = 1):
    global nextUpdate, mainId
    time = datetime.datetime.now().timestamp()
    while (not mustQuit and time < nextUpdate):
        time = datetime.datetime.now().timestamp()
    nextUpdate = datetime.datetime.now().timestamp() + animTime
    if (not mustQuit):
        drawMap()
        master.update()
        checkMap(rule)
        master.after(500, launchSim(rule, animTime))
    else:
        window.destroy()
        master.destroy()
        master.quit()

def clickCallBackEvent(event):
    global forestMap
    x = math.ceil(event.x / caseSize) - 1
    y =  math.ceil(event.y / caseSize) - 1
    forestMap[y][x] = Type.FIRE
    drawCase(x, y, getColor(Type.FIRE))
    master.update()

def onClosing():
    global mustQuit
    mustQuit = True

def main(): #simulation de la fonction main
    global nbLine, nbCol, caseSize, window, mainId
    # PARSE ARGS
    parser = argparse.ArgumentParser(description='Simulate forest fires')
    parser.add_argument('-rows', type=int, default=10, help="number of rows")
    parser.add_argument('-cols', type=int, default=10, help="number of lines")
    parser.add_argument('-cell_size', type=int, default=50, help="size of cell in pixel")
    parser.add_argument('-anim_length', type=int, default=1, help="length of aniamtion in second")
    parser.add_argument('-afforestation', type=float, default=0.75, help="pourcentage of tree on the map")
    parser.add_argument('-rule', type=int, default=0, help="id of fire propagation rule, 0 basic rule, 1 complex rule")
    args = parser.parse_args()
    nbLine = args.rows
    nbCol = args.cols
    caseSize = args.cell_size
    animTime = args.anim_length
    afforestation = args.afforestation
    rule = args.rule

    # INIT WINDOW AND GAME VALUES
    createMap(afforestation)
    master.geometry(str(nbCol * caseSize) + "x" + str(nbLine * caseSize))
    window = Canvas(master, width=nbCol * caseSize, height=nbLine * caseSize)
    window.pack(side="top", fill="both", expand=True)
    window.bind('<Button-1>', clickCallBackEvent)
    window.bind('<Button-3>', clickCallBackEvent)
    master.protocol("WM_DELETE_WINDOW", onClosing)
    master.after(500, launchSim(rule, animTime))

main()
